# CLI Webp Image Converter

This is a command-line interface (CLI) tool written in TypeScript that converts any image format to the WebP format.

## Requirements

Node.js v14 or higher

## Installation

Clone the repository:

```
git clone https://gitlab.com/qundola/cli-converter-to-webp.git
```

## Install dependencies:

```
cd cli-webp-image-converter
npm install
```

## Usage

The CLI tool accepts the path of the image file to be converted. The following command-line arguments are available:

```
npm run compile
node build/index.js <inputFilePath>
```

This will convert the image.jpg file to the WebP format and save it as image.webp in the ~/Pictures directory.
