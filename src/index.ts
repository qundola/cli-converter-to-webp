const converter = require('./converter');
const commander = require('commander');
const program = new commander.Command();

program
  .name('Webp converter.')
  .description('Script converting any image to webp file format.')
  .version('1.0.0')
  .argument('<path>', 'path to images folder');

program.parse();

program.args.length > 1
  ? console.log('Too match arguments!\nTry again.')
  : converter.convertToWebp(program.args[0]);
