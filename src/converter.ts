const fs = require('fs');
const path = require('path');

const TARGET_FOLDER_NAME = 'webp';
const isLinux: boolean = process.platform === 'linux' ? true : false;
const webp = require('webp-converter');
const FORMATS: string[] = [
  'jpg',
  'jpeg',
  'jfif',
  'pjpeg',
  'pjp',
  'gif',
  'png',
  'svg',
];

if (isLinux) webp.grant_permission(); // Fix Permission Issue.

function convertToWebp(sourceDirPath: string): void {
  if (!checkPath(sourceDirPath)) throw 'Error on the path!\nTry again.';

  const webpFolder: string = path.join(sourceDirPath, TARGET_FOLDER_NAME);

  if (!fs.existsSync(webpFolder)) fs.mkdirSync(webpFolder);

  fs.readdir(sourceDirPath, (_err: any, files: any[]) => {
    files.forEach(file => {
      const filePath: string = path.join(sourceDirPath, file);

      if (fs.statSync(filePath).isFile()) {
        const fileSize = fs.statSync(filePath).size;
        const [fileName, format] = file.split('.');
        const formatLower = format.toLowerCase();

        if (
          FORMATS.some(imgFormat => imgFormat === formatLower) &&
          fileSize > 0
        ) {
          const webpPath: string = path.join(webpFolder, fileName + '.webp');

          if (!fs.existsSync(webpPath)) webp.cwebp(filePath, webpPath, '-q 80');
        }
      }
    });
  });
}

function checkPath(path: string): boolean {
  if (!fs.existsSync(path)) return false;
  if (fs.statSync(path).isFile()) return false;
  return true;
}

module.exports.convertToWebp = convertToWebp;
